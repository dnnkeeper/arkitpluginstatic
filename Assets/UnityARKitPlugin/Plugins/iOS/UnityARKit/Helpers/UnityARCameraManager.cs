using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.iOS;

public class UnityARCameraManager : MonoBehaviour
{

    public Transform offsetOrigin, deviceTrackerTransform;
    public Camera m_camera;
    private UnityARSessionNativeInterface m_session;
    private Material savedClearMaterial;

    [Header("AR Config Options")]
    public UnityARAlignment startAlignment = UnityARAlignment.UnityARAlignmentGravity;
    public UnityARPlaneDetection planeDetection = UnityARPlaneDetection.Horizontal;
    public bool getPointCloud = true;
    public bool enableLightEstimation = true;
    public bool enableAutoFocus = true;
    public UnityAREnvironmentTexturing environmentTexturing = UnityAREnvironmentTexturing.UnityAREnvironmentTexturingNone;

    [Header("Image Tracking")]
    public ARReferenceImagesSet detectionImages = null;
    public int maximumNumberOfTrackedImages = 0;

    [Header("Object Tracking")]
    public ARReferenceObjectsSetAsset detectionObjects = null;
    private bool sessionStarted = false;

    public Dictionary<string, VirtualMarkerArKit> virtualMarkersDict
            = new Dictionary<string, VirtualMarkerArKit>();

    VirtualMarkerArKit lastTrackedMarker;

    Transform lastTrackedAnchor;

    public int markersCount;

    public GameObject FitToScanOverlay;

    bool hasAnyMarkers;

    public UnityEvent onHasMarkers, onZeroMarkers;

    public ARKitWorldTrackingSessionConfiguration sessionConfiguration
    {
        get
        {
            ARKitWorldTrackingSessionConfiguration config = new ARKitWorldTrackingSessionConfiguration();
            config.planeDetection = planeDetection;
            config.alignment = startAlignment;
            config.getPointCloudData = getPointCloud;
            config.enableLightEstimation = enableLightEstimation;
            config.enableAutoFocus = enableAutoFocus;
            config.maximumNumberOfTrackedImages = maximumNumberOfTrackedImages;
            config.environmentTexturing = environmentTexturing;
            if (detectionImages != null)
                config.referenceImagesGroupName = detectionImages.resourceGroupName;

            if (detectionObjects != null)
            {
                config.referenceObjectsGroupName = "";  //lets not read from XCode asset catalog right now
                config.dynamicReferenceObjectsPtr = m_session.CreateNativeReferenceObjectsSet(detectionObjects.LoadReferenceObjectsInSet());
            }

            return config;
        }
    }

    // Use this for initialization
    void Start()
    {
        virtualScenes = GameObject.FindGameObjectsWithTag("VirtualScene");

        m_session = UnityARSessionNativeInterface.GetARSessionNativeInterface();

        Application.targetFrameRate = 60;

        var config = sessionConfiguration;
        if (config.IsSupported)
        {
            m_session.RunWithConfig(config);
            UnityARSessionNativeInterface.ARFrameUpdatedEvent += FirstFrameUpdate;
        }

        if (m_camera == null)
        {
            m_camera = Camera.main;
        }


        var virtualMarkers = GameObject.FindObjectsOfType<VirtualMarkerArKit>();
        foreach (var virtualMarker in virtualMarkers)
        {
            if (virtualMarker.imageDatabase == detectionImages)
            {
                if (!virtualMarkersDict.ContainsKey(virtualMarker.imageName))
                {
                    virtualMarkersDict.Add(virtualMarker.imageName, virtualMarker);
                }
                else
                {
                    Debug.LogError("virtualMarkersDict already contains marker with databaseIndex " + virtualMarker.databaseIndex);
                }
            }
            else
            {
                Debug.LogError(virtualMarker + " imageDatabase missmatch! " + virtualMarker.imageDatabase + " != " + detectionImages);
            }

            virtualMarker.gameObject.SetActive(false);
        }

        if (!Application.isEditor)
        {
            SetVirtualSceneActive(false);
            onZeroMarkers.Invoke();
        }

        Debug.Log("Подписка");
        UnityARSessionNativeInterface.ARImageAnchorAddedEvent += AddImageAnchor;
        UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent += UpdateImageAnchor;
        UnityARSessionNativeInterface.ARImageAnchorRemovedEvent += RemoveImageAnchor;

        lastTrackedAnchor = new GameObject("LastTrackedAnchor").transform;

    }

    void AddImageAnchor(ARImageAnchor arImageAnchor)
    {
        markersCount++;

        Debug.LogFormat("image anchor added[{0}] : tracked => {1}", arImageAnchor.identifier, arImageAnchor.isTracked);

        VirtualMarkerArKit virtualMarker;
        if (virtualMarkersDict.TryGetValue(arImageAnchor.referenceImageName, out virtualMarker))
        {
            lastTrackedMarker = virtualMarker;

            var position = UnityARMatrixOps.GetPosition(arImageAnchor.transform);
            var rotation = UnityARMatrixOps.GetRotation(arImageAnchor.transform);

            var centerPoseRotationEuler = lastTrackedAnchor.transform.eulerAngles;
            var virtualMarkerRotationEuler = lastTrackedMarker.transform.rotation.eulerAngles;

            lastTrackedAnchor.SetPositionAndRotation(position, rotation);

            if (Mathf.Abs(virtualMarkerRotationEuler.x) > 1f || Mathf.Abs(virtualMarkerRotationEuler.z) > 1f)
            {
                //lastTrackedAnchor.transform.rotation = Quaternion.Euler(virtualMarkerRotationEuler.x, centerPoseRotationEuler.y, virtualMarkerRotationEuler.z);
            }
            else
            {
                lastTrackedAnchor.transform.rotation = Quaternion.Euler(virtualMarkerRotationEuler.x, centerPoseRotationEuler.y, virtualMarkerRotationEuler.z);

                //centerPoseRotationEuler.x = 0f;
                //centerPoseRotationEuler.z = 0f;
            }

            
        }
    }

    void UpdateImageAnchor(ARImageAnchor arImageAnchor)
    {
        //Debug.LogFormat("image anchor updated[{0}] : tracked => {1}", arImageAnchor.identifier, arImageAnchor.isTracked);

        var position = UnityARMatrixOps.GetPosition(arImageAnchor.transform);
        var rotation = UnityARMatrixOps.GetRotation(arImageAnchor.transform);

        lastTrackedAnchor.SetPositionAndRotation(position, rotation);

    }

    void RemoveImageAnchor(ARImageAnchor arImageAnchor)
    {
        markersCount--;

        Debug.LogFormat("image anchor removed[{0}] : tracked => {1}", arImageAnchor.identifier, arImageAnchor.isTracked);
    }

    void OnDestroy()
    {
        UnityARSessionNativeInterface.ARImageAnchorAddedEvent -= AddImageAnchor;
        UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent -= UpdateImageAnchor;
        UnityARSessionNativeInterface.ARImageAnchorRemovedEvent -= RemoveImageAnchor;

        m_session.Pause();
    }

    void FirstFrameUpdate(UnityARCamera cam)
    {
        Debug.Log("FirstFrameUpdate");
        sessionStarted = true;
        UnityARSessionNativeInterface.ARFrameUpdatedEvent -= FirstFrameUpdate;
    }

    public void SetCamera(Camera newCamera)
    {
        if (m_camera != null)
        {
            UnityARVideo oldARVideo = m_camera.gameObject.GetComponent<UnityARVideo>();
            if (oldARVideo != null)
            {
                savedClearMaterial = oldARVideo.m_ClearMaterial;
                Destroy(oldARVideo);
            }
        }
        SetupNewCamera(newCamera);
    }

    private void SetupNewCamera(Camera newCamera)
    {
        m_camera = newCamera;

        if (m_camera != null)
        {
            UnityARVideo unityARVideo = m_camera.gameObject.GetComponent<UnityARVideo>();
            if (unityARVideo != null)
            {
                savedClearMaterial = unityARVideo.m_ClearMaterial;
                Destroy(unityARVideo);
            }
            unityARVideo = m_camera.gameObject.AddComponent<UnityARVideo>();
            unityARVideo.m_ClearMaterial = savedClearMaterial;
        }
    }

    GameObject[] virtualScenes;

    // Update is called once per frame
    void Update()
    {
        if (m_camera != null && sessionStarted)
        {
            // JUST WORKS!
            Matrix4x4 matrix = m_session.GetCameraPose();
            deviceTrackerTransform.transform.localPosition = UnityARMatrixOps.GetPosition(matrix);
            deviceTrackerTransform.transform.localRotation = UnityARMatrixOps.GetRotation(matrix);

            if (lastTrackedMarker != null)
            {
                offsetOrigin.SetPositionAndRotation(
                                        lastTrackedMarker.transform.TransformPoint(lastTrackedAnchor.transform.InverseTransformPoint(deviceTrackerTransform.position)),
                                        lastTrackedMarker.transform.rotation * (Quaternion.Inverse(lastTrackedAnchor.transform.rotation) * deviceTrackerTransform.rotation));
            }

            var prjMat = m_session.GetCameraProjection();
            var fov_y = Mathf.Atan(1 / prjMat[5]) * 2f * Mathf.Rad2Deg;
            m_camera.fieldOfView = fov_y;

            //m_camera.projectionMatrix = m_session.GetCameraProjection();
        }

        if (!Application.isEditor)
        {

            if (markersCount > 0)
            {
                FitToScanOverlay.SetActive(false);

                if (!hasAnyMarkers)
                {
                    hasAnyMarkers = true;
                    SetVirtualSceneActive(true);
                    onHasMarkers.Invoke();
                }
            }
            else
            {
                FitToScanOverlay.SetActive(true);

                if (hasAnyMarkers)
                {
                    markersCount = 0;
                    SetVirtualSceneActive(false);
                    onZeroMarkers.Invoke();
                }
            }
        }

    }

    public void SetVirtualSceneActive(bool b)
    {
        foreach (var virtualScene in virtualScenes) //GameObject.FindGameObjectsWithTag("VirtualScene"))
        {
            virtualScene.SetActive(b);
        }
    }

}
