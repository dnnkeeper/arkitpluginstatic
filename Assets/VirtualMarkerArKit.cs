﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.XR.iOS;

public class VirtualMarkerArKit : MonoBehaviour
{
    public int databaseIndex;

    public string imageName;

    public ARReferenceImagesSet imageDatabase;

    private Material _imageMaterial;
    private Material imageMaterial
    {
        get
        {
            if (_imageMaterial == null)
            {
                _imageMaterial = new Material(Shader.Find("Diffuse"));
            }
            return _imageMaterial;
        }
    }

#if UNITY_EDITOR

    private void Reset()
    {
        UpdateImage();
    }

    [ContextMenu("UpdateImageByIndex")]
    public void UpdateImage()
    {
        UpdateImage(databaseIndex);
    }

    public void UpdateImage(int idx)
    {
        //var imageDatabase = FindObjectOfType<ARCoreSession>().SessionConfig.AugmentedImageDatabase;

        var imageGameObject = transform.Find("MarkerRenderer");
        if (imageGameObject == null)
        {
            var newGameObject = GameObject.CreatePrimitive(PrimitiveType.Quad);
            newGameObject.name = "MarkerRenderer";
            imageGameObject = newGameObject.transform;
        }

        imageGameObject.parent = transform;
        imageGameObject.localPosition = Vector3.zero;
        imageGameObject.localRotation = Quaternion.Euler(90f, 0f, 0f);
        imageGameObject.localScale = Vector3.one * 0.3f;

        if (imageDatabase != null && idx >= 0 && imageDatabase.referenceImages.Length > idx)
        {
            var imgInfo = imageDatabase.referenceImages[idx];
            if (imgInfo.imageTexture != null)
            {
                imageMaterial.mainTexture = imgInfo.imageTexture;
                imageGameObject.GetComponent<Renderer>().material = imageMaterial;
                if (imgInfo.physicalSize != 0f)
                {
                    imageGameObject.localScale = new Vector3(imgInfo.physicalSize, imgInfo.physicalSize, 1f);

                }
                Debug.Log("imgInfo.physicalSize " + imgInfo.physicalSize);
            }
            else
            {
                Debug.LogWarning("imageDatabase[idx].Texture == null");
            }

            imageName = imgInfo.imageName;
        }
        else
        {
            imageMaterial.mainTexture = null;
            imageGameObject.GetComponent<Renderer>().material = imageMaterial;
        }
    }
#endif
}
